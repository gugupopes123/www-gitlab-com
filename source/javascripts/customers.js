import $ from 'jquery';
import 'select2'; 

(function() {
  var TILES_PER_PAGE = 16;

  var sortPagination = function(total, page) {
    var items = [];
    if (page > 1) items.push({ title: '<<', where: 1 });
    if (page > 1) {
      items.push({ title: '<', where: page - 1 });
    }

    if (page > 6) items.push({ title: '...', separator: true });

    var start = Math.max(page - 4, 1);
    var end = Math.min(page + 4, total);

    for (var i = start; i <= end; i++) {
      var isActive = i === page;
      items.push({ title: i, active: isActive, where: i });
    }

    if (total - page > 4) items.push({ title: '...', separator: true });

    if (total - page >= 1) {
      items.push({ title: '>', where: page + 1 });
    }

    if (total - page >= 1) items.push({ title: '>>', where: total });

    return items;
  };

  var CustomerListHandler = (function() {
    function CustomerListHandler() {
      this.$industryFilter = $('select[name=filter-industry]').select2();
      this.$solutionFilter = $('select[name="filter-solution"]').select2();
      this.$topicFilter = $('select[name="filter-topic"]').select2();
      this.$orgList = $('.org-image', '.org-groups-container');
      this.$orgContainer = $('.org-groups-container');
      // $(this.$filters[0]).attr('checked', 'checked');

      // binds
      this.$industryFilter.on('change', this.render.bind(this));
      this.$solutionFilter.on('change', this.render.bind(this));
      this.$topicFilter.on('change', this.render.bind(this));
      this.render.call(this);
    }

    CustomerListHandler.prototype.buildFilterSelector = function() {
      var industryValue = this.$industryFilter.val();
      var solutionValue = this.$solutionFilter.val();
      var topicValue    = this.$topicFilter.val();
      var selector      = '';

      if (industryValue) {
        selector += '[data-industry-type="' + industryValue + '"]';
      }
      if (solutionValue) {
        selector += '[data-solutions*="||' + solutionValue + '||"]';
      }
      if (topicValue) {
        selector += '[data-topics*="||' + topicValue + '||"]';
      }

      return selector;
    };

    CustomerListHandler.prototype.filterData = function() {
      var filterSelector = this.buildFilterSelector.call(this);
      if (filterSelector === '') {
        this.$filteredData = this.$orgList;
      } else {
        this.$filteredData = this.$orgList.filter(filterSelector);
      }
    };

    CustomerListHandler.prototype.render = function(e) {
      this.filterData.call(this);
      function renderData(pageNumber) {
        if (!pageNumber) {
          this.currentPage = 1;
          this.renderOrgList.call(this);
        } else {
          var newPageNumber;
          if (pageNumber.text().toLowerCase() === '>') {
            newPageNumber = this.currentPage += 1;
          } else if (pageNumber.text().toLowerCase() === '<') {
            newPageNumber = this.currentPage -= 1;
          } else if (pageNumber.text().toLowerCase().indexOf('<<') !== -1) {
            newPageNumber = this.currentPage = 1;
          } else if (pageNumber.text().toLowerCase().indexOf('>>') !== -1) {
            newPageNumber = this.getTotalPages();
          } else {
            newPageNumber = parseInt(pageNumber.text(), 10);
          }
          this.currentPage = newPageNumber;
          this.renderOrgList.call(this);
        }
      }
      if (e != null) {
        var $callingElement = $(e.currentTarget);
        if ( !$callingElement.is('select') ) {
          renderData.call(this, $callingElement);
        } else {
          renderData.call(this);
        }
      } else {
        renderData.call(this);
      }
    };

    CustomerListHandler.prototype.renderOrgList = function() {
      this.$orgContainer.empty();
      $('.pagination li span').off('click');
      document.querySelector('.pagination').innerHTML =
      sortPagination(this.getTotalPages(), this.currentPage)
        .map(function(e) {
          var activeElement;
          if (e.active) {
            activeElement = '<li seperator="' + e.seperator + '" class="active" disabled="' + e.disabled + '">'
                            + '<span>' + e.title + '</span>'
                            + '</li>';
          } else {
            activeElement = '<li seperator="' + e.seperator + '" disabled="' + e.disabled + '">'
                            + '<span>' + e.title + '</span>'
                            + '</li>';
          }
          return activeElement;
        }).join('');
      $('.pagination li:not(.active) span').on('click', this.render.bind(this));

      // The organization table
      for (
        var i = (this.currentPage - 1) * TILES_PER_PAGE;
        i < (this.currentPage * TILES_PER_PAGE);
        i += 1
      ) {
        if (this.$filteredData[i] != null) {
          this.$orgContainer.append(this.$filteredData[i]);
        }
      }
    };

    CustomerListHandler.prototype.getTotalPages = function() {
      return Math.ceil(this.$filteredData.length / TILES_PER_PAGE);
    };

    return CustomerListHandler;
  })();

  new CustomerListHandler();
})();