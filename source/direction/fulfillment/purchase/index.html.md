---
layout: markdown_page
title: "Group Direction - Purchase"
description: This is the direction page for the Purchase group which is part of the Fulfillment stage. Learn more here!
canonical_path: "/direction/fulfillment/purchase/"
---

- TOC
{:toc}


### Introduction and how you can help
Thanks for visiting the direction page for the Provision group within the Fulfillment Section in GitLab. This page is being actively maintained by [Tatyana Golubeva](https://gitlab.com/tgolubeva). This vision and direction is a work in progress and sharing your feedback directly on issues and epics on GitLab.com is the best way to contribute.


### Overview
The Purchase group within the Fulfillment Section takes care of activities related to a purchase (managing subscription, understanding factors that lead to a buying decision like billable users, account management) and the purchase flows themselves (including trials). This includes the invisible purchase transactions processing and communication to/from [Zuora](https://gitlab.com/gitlab-org/customers-gitlab-com/-/tree/staging/doc/architecture#zuora), our subscription system. Purchase flows and subscription management are currently maintained in [CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com/-/tree/staging/doc/architecture#customersdot).

#### Engineering

The Fulfillment sub-department handbook page has a [purchase section](/handbook/engineering/development/fulfillment/#purchase)

#### Challenges to address
There are two main problems we are focused on at the moment in the Purchase group. 

1. Optimizing the customer experience in transacting with us.
1. Giving customers ready access to the tools they need to manage their subscriptions and make informed buying decisions. 

We will be improving in these areas through much of the work slated in our [Fulfillment OKRs](https://gitlab.com/gitlab-org/growth/product/-/issues/1624), along with our concerted effort to improve the percentage of transactions processed via self-service through our new [company KPI](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/65968).








