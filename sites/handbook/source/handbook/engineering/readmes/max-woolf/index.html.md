---
layout: markdown_page
title: "Max Woolf's README"
job: "Senior Backend Engineer"
---

## Max Woolf's README

**Max Woolf - Senior Backend Engineer, Manage:Compliance**

Hey there 👋

This is the README for Max Woolf, Senior Backend Engineer in the Manage::Compliance group. The purpose of this document is to, hopefully, give you some knowledge about me (both personal and professional), the way I work and my personality before meeting me for the first time. 

## Personally

- I was born, raised and now currently live in Birmingham, England. Famous for [chocolate](https://en.wikipedia.org/wiki/Cadbury), [canals](https://en.wikipedia.org/wiki/Birmingham_Canal_Navigations) (we have more than Venice!) and [Black Sabbath](https://en.wikipedia.org/wiki/Black_Sabbath) (amongst other things).
- Me and my wife, Ciara, moved here in 2014 after a few years studying in Brighton and working in Cardiff, Wales.
- I graduated from the University of Sussex in Brighton in 2012 and studied [Music Informatics](https://en.wikipedia.org/wiki/Music_informatics). Creativity in software engineering is really important to me.
- We live with our dog, Winston, who we adopted in 2018. In my new remote job, he is my official foot-warmer. This is, admittedly, a slight downgrade from his planned life of being a [Guide Dog](https://www.guidedogs.org.uk/).
- I'm a charity trustee for my [local park](http://cotteridgepark.org.uk) and recently helped build a small community building and café.
- I suffer from [Obsessive-Compulsive Disorder (OCD)](https://en.wikipedia.org/wiki/Obsessive%E2%80%93compulsive_disorder). I'm always happy to discuss it with friends, family and colleagues. I can often be found taking part in discussions in `#mental_health_aware` in Slack.
Anyone (that means **you**) are welcome to reach out to me privately to discuss mental health issues at any time.
- I'm _super_ dubious about the value of personality tests, but apparently [I'm an Architect (INTJ-T)](https://www.16personalities.com/intj-personality). 
- I am an introvert. I enjoy social interaction, but it exhausts me.
- At the start of the COVID-19 pandemic, I was one of the first 10,000 people in the world to receive [the ChAdOx1 vaccine](https://en.wikipedia.org/wiki/Oxford%E2%80%93AstraZeneca_COVID-19_vaccine) as part of a clinical trial. 

## Professionally

* I started working at GitLab in April 2020 (when literally _nothing_ else was going on in the world) and have been working in the Manage::Compliance team ever since.
* Prior to working at GitLab, I worked for a small health technology start-up and several digital agencies.
* GitLab is, by far, the largest company I have ever worked for. Previously, the largest company I worked for had ~50 employees.
* Writing software is what I love doing. I'm incredibly fortunate that the venn diagram of "Things Max likes to do" and "Skills that are in demand" are just two perfectly overlapping circles.

### What do I actually do?

My working days tend to contain the same sorts of tasks, just in a different order or balance.

* The majority of my daily work is _writing code_ in some form. Usually this takes the form of feature development or paying down technical debt.
* Another large part of my role is _reviewing code_. As a backend maintainer, backend reviewer and merge request coach I typically spend ~20% of my time reviewing contributions from other engineers or from the GitLab community.
* The rest of my time is spent **learning**, **mentoring** and **spending time socially with my colleagues.**

## What I assume about others

* Everyone has something to teach me. Sometimes, but not always, I have something to teach them.
* [Everyone is intelligent and well-meaning.](https://github.com/thoughtbot/guides/tree/master/code-review#everyone)
* You are excellent at what you do.

## Communicating with me

* I'm usually actively working between 8am - 5pm UK time. This can shift +/- 2 hours. I'm happy to work anytime between 7am - 11pm on occasion for a customer or colleague, but I try not to make a habit of it. (The value of my contributions at 11pm are probably questionable. 😅)
* I'm _always_ available for coffee chats. One of the benefits of working at GitLab is meeting people from across the world and I want to! **Don't be shy, add a coffee chat to my calendar.**
* In general, I prefer Slack over Email for anything that doesn't belong in an Issue or Merge Request. I will often mark my email inbox as read and pick up tasks from my GitLab todo list.
* **Don't hesitate to ask me anything.** If I can help, I will! For a quick response, find me on Slack during my working hours.
* My sense of humour is [very typically British](https://en.wikipedia.org/wiki/British_humour) or "dry". I like to think I'm good at regulating it when it may not be appropriate...

## Things I'm trying to improve

* I'm not great at reading between the lines as a way of accepting criticism or constructive feedback. **If something is wrong, please say so.** I will thank you for it!
* I can sometimes act too much like a magpie; paying too much attention to the "new and shiny" things and not enough to the "older but more reliable" things.

## Finding me

You can find me in various places both within GitLab and online.

* [LinkedIn](https://www.linkedin.com/in/max-woolf-488bb534/)
* [Personal CV & Blog](https://max.woolf.io)
* [Instagram](https://instagram.com/max.woolf)

Non-employees can drop me an email (mwoolf@gitlab.com) and I'll try to respond as quickly as possible.

GitLab employees can find me in Slack, either via DM or I'm active in the following channels (amongst others).

| Work | Social |
| ---- | ------ |
| `#s_manage` | `#dog` |
| `#g_manage_compliance` | `#mental_health_aware` |
| `#f_graphql` | `#coffee-and-tea` |
| `#development` | `#dad_jokes` |
| `#backend` | `#uk` |
